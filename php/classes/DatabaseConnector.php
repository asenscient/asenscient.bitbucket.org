<?php

include $_SERVER['DOCUMENT_ROOT'] . '/survey/php/constants/DatabaseInfo.php';

class DatabaseConnector
{
	protected $connection;
	protected $query;

	//Connect with the database.
	public function Connect()
	{
		try
		{
			$this->connection = new PDO("mysql:host=". HOST .";dbname=". DB ."", USER, PASS);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $ex)
		{
			$this->Error("Exception has risen: " . $ex);
		}

		if(!$this->connection)
		{
		   $this->Error("Connection could not be established");
		}
	}

	//Disconnect from the server
	public function Disconnect()
	{
		$this->connection = null;
	}

	//Upon destroying the class instance, clear the connection.
	public function __destruct()
	{
		$this->Disconnect();
	}

	//Prepare query to prevent sql injection
	function Query($q)
	{
		$this->Connect();

		if($this->connection)
		{
			try
			{
				$this->query = $this->connection->prepare($q);
			}
			catch(PDOException $e)
			{
				$this->Error("Could not prepare query: " . $e);
			}
		}
		else
		{
			$this->Error("Connection was closed upon requesting a action");
		}

		$this->Disconnect();
	}

	//Bind parameter in query
	function Bind($param, $value, $type=PDO::PARAM_NULL)
	{
		if(!$this->query)
		{
			$this->Error("No query to bind params to");
		}

	    try
	    {
	        $this->query->bindValue($param, $value, $type);
	    }
	    catch(PDOException $e)
	    {
	         $this->Error("Could not bind params: " . $e);
	    }
	}

	// Run select query
	public function Select()
	{
		$this->Connect();

		if($this->connection)
		{
			try
			{
				$this->query->execute();
				return $this->query->fetch(PDO::FETCH_ASSOC);
			}
			catch(PDOException $e)
			{
				$this->Error("Could not execute the query: " . $e);
			}
		}
		else
		{
			$this->Error("Connection was closed upon requesting a action");
		}

		$this->Disconnect();
	}

	// Run select query with multiple results.
	public function SelectMulti()
	{
		$this->Connect();

		if($this->connection)
		{
			try
			{
				$this->query->execute();
				return $this->query->fetchAll(PDO::FETCH_ASSOC);
			}
			catch(PDOException $e)
			{
				$this->Error("Could not execute the query: " . $e);
			}
		}
		else
		{
			$this->Error("Connection was closed upon requesting a action");
		}

		$this->Disconnect();
	}

	// Run query without output. ( Applies for query that dont return a value )
	public function Run()
	{
		$this->Connect();

		if($this->connection)
		{
			try
			{
				$this->query->execute();
				if($this->query)
				{
				   return true;
				}
				else
				{
			       return false;
				}
			}
			catch(PDOException $e)
			{
				$this->Error("Could not execute the query: " . $e);
			}
		}
		else
		{
			$this->Error("Connection was closed upon requesting a action");
		}

		$this->Disconnect();
	}

	// Count rows
	public function Count()
	{
		$this->Connect();

		if($this->connection)
		{
			try
			{
				$this->query->execute();
				return $this->query->rowCount();
			}
			catch(PDOException $e)
			{
				$this->Error("Could not execute the query: " . $e);
			}
		}
		else
		{
			$this->Error("Connection was closed upon requesting a action");
		}

		$this->Disconnect();
	}


	//Checks if debug is enable, determines the style of error message.
	function Error($message, $fatal=true)
	{
		if(DEBUG == "true")
		{
			echo $message;

			if($fatal)
			{
				$this->Disconnect();
				exit();
			}	
		}
		else
		{
			echo "An exception was risen, please contact website administrator.";
			if($fatal)
			{
				$this->Disconnect();
				exit();
			}
		}
	}
}


?>